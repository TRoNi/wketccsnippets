// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    let disposableTestDebug = vscode.commands.registerCommand('extension.LaunchFileTestsDebug', (input: vscode.Uri) => {

        let testTerminal: vscode.Terminal = null;
        let myContext: vscode.ExtensionContext = context;
        let filename:string = input.path.replace(/^.*[\\\/]/, '');
        filename = filename.replace(/\.[^/.]+$/, "");

        testTerminal = vscode.window.createTerminal('file test');
        testTerminal.show();
        testTerminal.sendText(`npm run testfile:debug -- --name ${filename}`);
    });

    let disposableTest = vscode.commands.registerCommand('extension.LaunchFileTests', (input: vscode.Uri) => {

        let testTerminal: vscode.Terminal = null;
        let myContext: vscode.ExtensionContext = context;
        let filename:string = input.path.replace(/^.*[\\\/]/, '')
        filename = filename.replace(/\.[^/.]+$/, "");

        testTerminal = vscode.window.createTerminal('file test');
        testTerminal.show();
        testTerminal.sendText(`npm run testfile -- --name ${filename}`);
    });

    context.subscriptions.push(disposableTest);
    context.subscriptions.push(disposableTestDebug);
}