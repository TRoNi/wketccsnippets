# WkTccSnippets

This extension for Visual Studio Code adds snippets for Angular for TypeScript and HTML Snippets for WKE TCC vscode development enviroment.

It also adds some useful utilities to the vscode, like easily test one file with karma

# Features

  - Typescript snippets
  - HTML snippets
  - TCC import service and component snippets
  - Karma one file testing

# Usage

## Karma one file testing

Go to vscode explorer content menu and double click in a .ts file. A new terminal will open and execute the tests.

## TypeScript Snippets

| Snippet                      | Purpose                          |
|------------------------------|----------------------------------|
| `wk-mod`                     | module                           |
| `wk-comp`                    | component                        |
| `wk-serv`                    | service                          |
| `wk-imp`                     | import file                      |
| `wk-gs`                      | private variable + getter&setter |
| `wk-get`                     | private variable + getter        |
| `wk-set`                     | private variable + setter        |
| `wk-pubf`                    | public class member              |
| `wk-intf`                    | exportable interface             |

## HTML Snippets

| Snippet                      | Purpose                          |
|------------------------------|----------------------------------|
|                              |                                  |

## TCC compoment and services Snippets

| Snippet                      | Purpose                          |
|------------------------------|----------------------------------|
|                              |                                  |

Alternatively, press `Ctrl`+`Space` to activate snippets from within the editor.

# Known Issues

Not HTML and TCC comp and service snippets right now.

# Release Notes

## 1.4.0

Solved test file not working

=======
## 1.2.0

Remove "-" from prefixes so it is compatible with VS

## 1.1.0

Added test with debug option

## 1.0.1
>>>>>>> 98d78ee000d11958031a532c2604d48f17d11fc0

Update project readme file

## 1.0.0

Initial release of the extension. Some snippets and initial support for testing one file

# Installation

1. Install Visual Studio Code 1.17.0 or higher
1. Launch Code
1. From the command palette `Ctrl`-`Shift`-`P` (Windows, Linux) or `Cmd`-`Shift`-`P` (OSX)
1. Select `Instalar desde VSIX`
1. Select the extension VSIX file
1. Reload Visual Studio Code
