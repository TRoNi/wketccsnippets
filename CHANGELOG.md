## Change log

### 1.4.0

Solved test file not working

### 1.2.0

Remove "-" from prefixes so it is compatible with VS

### 1.1.0

Added test with debug option

### 1.0.1

Update project readme file

### 1.0.0

Initial release of the extension. Some snippets and initial support for testing one file